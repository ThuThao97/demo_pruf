package com.example.admin.myapplication.activity;

import android.animation.Animator;
import android.animation.AnimatorSet;
import android.animation.ObjectAnimator;
import android.content.Intent;
import android.net.Uri;
import android.os.Bundle;
import android.os.Handler;
import android.support.v7.app.AppCompatActivity;
import android.view.View;
import android.view.animation.AccelerateDecelerateInterpolator;
import android.widget.ImageView;

import com.example.admin.myapplication.R;
import com.example.admin.myapplication.widges.RippleBackground;

import java.util.ArrayList;

public class ContactActivity extends AppCompatActivity implements View.OnClickListener {
    private ImageView imgBack, imgCenter, imgZalo, imgFaceBook, imgMail, imgPhone;
    private RippleBackground bg_Content;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_contact);
        setupView();
        onClickListerner();
    }

    private void setupView() {
        imgBack = findViewById(R.id.back);
        imgCenter = findViewById(R.id.imgCenter);
        imgZalo = findViewById(R.id.imgZalo);
        imgFaceBook = findViewById(R.id.imgFaceBook);
        imgMail = findViewById(R.id.imgMail);
        imgPhone = findViewById(R.id.imgPhone);
        bg_Content = findViewById(R.id.bg_Content);

        starAnimation();
        getCommunication();

    }

    //Hàm này để show ra các img sau khi hệ thống load được 3s
    private void starAnimation() {
        final Handler handler = new Handler();
        bg_Content.startRippleAnimation();
        handler.postDelayed(new Runnable() {
            @Override
            public void run() {
                showZalo();
                showFaceBook();
                showMail();
                showPhone();
            }
        }, 3000);
    }

    private void showZalo() {
        AnimatorSet animatorSet = new AnimatorSet();
        animatorSet.setDuration(400);
        animatorSet.setInterpolator(new AccelerateDecelerateInterpolator());
        ArrayList<Animator> animatorList = new ArrayList<Animator>();
        ObjectAnimator scaleXAnimator = ObjectAnimator.ofFloat(imgZalo, "ScaleX", 0f, 1.2f, 1f);
        animatorList.add(scaleXAnimator);
        ObjectAnimator scaleYAnimator = ObjectAnimator.ofFloat(imgZalo, "ScaleY", 0f, 1.2f, 1f);
        animatorList.add(scaleYAnimator);
        animatorSet.playTogether(animatorList);
        imgZalo.setVisibility(View.VISIBLE);
        animatorSet.start();
    }

    private void showFaceBook() {
        AnimatorSet animatorSet = new AnimatorSet();
        animatorSet.setDuration(400);
        animatorSet.setInterpolator(new AccelerateDecelerateInterpolator());
        ArrayList<Animator> animatorList = new ArrayList<Animator>();
        ObjectAnimator scaleXAnimator = ObjectAnimator.ofFloat(imgFaceBook, "ScaleX", 0f, 1.2f, 1f);
        animatorList.add(scaleXAnimator);
        ObjectAnimator scaleYAnimator = ObjectAnimator.ofFloat(imgFaceBook, "ScaleY", 0f, 1.2f, 1f);
        animatorList.add(scaleYAnimator);
        animatorSet.playTogether(animatorList);
        imgFaceBook.setVisibility(View.VISIBLE);
        animatorSet.start();
    }

    private void showMail() {
        AnimatorSet animatorSet = new AnimatorSet();
        animatorSet.setDuration(400);
        animatorSet.setInterpolator(new AccelerateDecelerateInterpolator());
        ArrayList<Animator> animatorList = new ArrayList<Animator>();
        ObjectAnimator scaleXAnimator = ObjectAnimator.ofFloat(imgMail, "ScaleX", 0f, 1.2f, 1f);
        animatorList.add(scaleXAnimator);
        ObjectAnimator scaleYAnimator = ObjectAnimator.ofFloat(imgMail, "ScaleY", 0f, 1.2f, 1f);
        animatorList.add(scaleYAnimator);
        animatorSet.playTogether(animatorList);
        imgMail.setVisibility(View.VISIBLE);
        animatorSet.start();
    }

    private void showPhone() {
        AnimatorSet animatorSet = new AnimatorSet();
        animatorSet.setDuration(400);
        animatorSet.setInterpolator(new AccelerateDecelerateInterpolator());
        ArrayList<Animator> animatorList = new ArrayList<Animator>();
        ObjectAnimator scaleXAnimator = ObjectAnimator.ofFloat(imgPhone, "ScaleX", 0f, 1.2f, 1f);
        animatorList.add(scaleXAnimator);
        ObjectAnimator scaleYAnimator = ObjectAnimator.ofFloat(imgPhone, "ScaleY", 0f, 1.2f, 1f);
        animatorList.add(scaleYAnimator);
        animatorSet.playTogether(animatorList);
        imgPhone.setVisibility(View.VISIBLE);
        animatorSet.start();
    }

    public void onClickListerner() {
        imgBack.setOnClickListener(this);
        imgPhone.setOnClickListener(this);
        imgMail.setOnClickListener(this);
        imgFaceBook.setOnClickListener(this);
        imgZalo.setOnClickListener(this);

    }

    @Override
    public void onClick(View v) {
        switch (v.getId()) {
            case R.id.back: {
                Intent intent = new Intent(ContactActivity.this, LoginActivity.class);
                startActivity(intent);
            }
            break;
            case R.id.imgPhone: {
                Intent intent = new Intent(Intent.ACTION_DIAL);
                intent.setData(Uri.parse("tel: 0374934435"));
                startActivity(intent);
            }
            break;
            case R.id.imgMail: {
                Intent intent = new Intent(Intent.ACTION_SEND);
                intent.setType("plain/text");
                intent.putExtra(Intent.EXTRA_EMAIL, new String[]{"thaorii435@gmail.com"});
                intent.putExtra(Intent.EXTRA_SUBJECT, "");
                intent.putExtra(Intent.EXTRA_TEXT, "");
                startActivity(intent.createChooser(intent, ""));
            }
            break;
            case R.id.imgFaceBook: {
                try {
                    Intent intent = new Intent(Intent.ACTION_VIEW, Uri.parse("fb://profile/MY_PAGE_ID"));
                    startActivity(intent);
                } catch (Exception e) {
                    startActivity(new Intent(Intent.ACTION_VIEW, Uri.parse("https://www.facebook.com/thuthao.dang.750" +
                            "")));
                }
            }
            break;
            case R.id.imgZalo: {
                try {
                    Intent intent = new Intent(Intent.ACTION_VIEW, Uri.parse("https://zalo.me/" + "0986584203"));
                    startActivity(intent);
                } catch (Exception e) {
                    e.printStackTrace();
                }

            }
        }
    }

    public void getCommunication() {


    }
}
