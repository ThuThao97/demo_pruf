package com.example.admin.myapplication.activity;

import android.annotation.SuppressLint;
import android.app.Activity;
import android.content.Intent;
import android.content.SharedPreferences;
import android.content.res.Configuration;
import android.media.MediaPlayer;
import android.net.Uri;
import android.os.Handler;
import android.support.design.widget.TextInputLayout;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.LinearLayout;
import android.widget.Switch;
import android.widget.TextView;
import android.widget.VideoView;

import com.example.admin.myapplication.R;

import java.util.Locale;

public class LoginActivity extends AppCompatActivity implements View.OnClickListener {
    private TextView Office;
    private VideoView videoView;
    private Switch aSwitch;
    private TextInputLayout textUserName, textPassWord;
    private EditText username, password;
    private LinearLayout layout_Office;
    private LinearLayout layout_Contact;
    private Button btnLogin;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_login);

        onSetUpview();
        onClickListener();
        loadLocale();
    }

    @SuppressLint("WrongViewCast")
    private void onSetUpview() {
        textUserName = findViewById(R.id.text_UserName);
        textPassWord = findViewById(R.id.text_PassWord);
        Office = findViewById(R.id.text_Office);
        username = findViewById(R.id.UserName);
        password = findViewById(R.id.Password);
        videoView = findViewById(R.id.videoview);
        aSwitch = findViewById(R.id.switch1);
        btnLogin=findViewById(R.id.btnLogin);
        layout_Office = findViewById(R.id.layout_Office);
        layout_Contact = findViewById(R.id.layout_Contact);
        setBackGroudVideo("https://storage.googleapis.com/coverr-main/mp4/Mt_Baker.mp4");
    }

    private void onClickListener() {
        aSwitch.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                changLanguage();
            }
        });
        layout_Office.setOnClickListener(this);
        layout_Contact.setOnClickListener(this);
        btnLogin.setOnClickListener(this);
    }

    @Override
    public void onClick(View v) {
        switch (v.getId()) {
            case R.id.layout_Office: {
                Intent intent = new Intent(this, Location.class);
                startActivity(intent);
                finish();
            }
            break;
            case R.id.layout_Contact: {
                Intent intent = new Intent(this, ContactActivity.class);
                startActivity(intent);
            }
            break;
            case R.id.btnLogin:{
                Intent intent=new Intent(this,MyProfileActivity.class);
                startActivity(intent);
            }
            break;
        }
    }

    private void changLanguage() {
        if (aSwitch.isChecked()) {
            setLocate("en");
            recreate();
        } else {
            setLocate("vi");
            recreate();
        }
    }

    @Override
    protected void onResume() {
        super.onResume();
    }

    private void setBackGroudVideo(String url) {
        try {
            videoView.setVideoURI(Uri.parse(url));
            videoView.start();
            //Log.e("Url video", url);
            videoView.setOnPreparedListener(new MediaPlayer.OnPreparedListener() {
                public void onPrepared(MediaPlayer mp) {
                    new Handler().postDelayed(new Runnable() {
                        @Override
                        public void run() {
                            videoView.setBackgroundDrawable(null);
                        }
                    }, 100);
                }
            });
        } catch (Exception e) {
            e.printStackTrace();
        }
    }
//        change language


    public void setLocate(String lang) {
        try {
            if (lang != null && !lang.equals("")) {
                Locale locale = new Locale(lang);
                Locale.setDefault(locale);
                Configuration configuration = new Configuration();
                configuration.locale = locale;
                getResources().updateConfiguration(configuration, getResources().getDisplayMetrics());
                // save data
                SharedPreferences.Editor editor = getSharedPreferences("Settings", MODE_PRIVATE).edit();
                editor.putString("My_lang", lang);
                editor.apply();
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    public void loadLocale() {
        try {
            SharedPreferences preferences = getSharedPreferences("Settings", Activity.MODE_PRIVATE);
            String lang = preferences.getString("My_lang", "");
            setLocate(lang);
        } catch (Exception e) {
            e.printStackTrace();
        }
    }


}









