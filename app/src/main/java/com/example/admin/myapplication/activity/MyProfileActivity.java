package com.example.admin.myapplication.activity;

import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.text.SpannableString;
import android.text.style.UnderlineSpan;

import com.example.admin.myapplication.R;
import com.example.admin.myapplication.widges.TextViewPlus;

public class MyProfileActivity extends AppCompatActivity {
    private TextViewPlus tvp_mk,cmnd,email,sdt,address,code_fast;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_my_profile);

        tvp_mk=findViewById(R.id.tvp_mk);
        code_fast=findViewById(R.id.code_login);
        email=findViewById(R.id.email);
        sdt=findViewById(R.id.sdt);
        address=findViewById(R.id.address);
        cmnd=findViewById(R.id.cmnd);
        SpannableString content=new SpannableString("Thay đổi");
        content.setSpan(new UnderlineSpan(),0,content.length(),0);
        tvp_mk.setText(content);
        code_fast.setText(content);
        email.setText(content);
        sdt.setText(content);
        address.setText(content);
        cmnd.setText(content);
    }
}
